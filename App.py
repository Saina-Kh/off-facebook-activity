import eel
import json
import pandas as pd



with open('Web/your_off-facebook_activity.json', encoding='utf8'  ) as f:
  data = json.load(f)
entries = []
for app in data['off_facebook_activity']:
    name = app['name']
    for event in app['events']:
        entries.append([name, event['type'], event['timestamp']])

OFA = pd.DataFrame(entries)

OFA.columns = ['app_name','event_type', 'timestamp']
OFA['event_type'].unique()

print('Number of unique event types:', len(OFA['event_type'].unique()))

print('Event types')
OFA['event_type'].unique()
OFA

import numpy as np
applications,NumberOfUses = np.unique(OFA['app_name'],return_counts = True)

import csv
myFile = open('Web/NewApplications.csv', 'w',encoding='utf8'  )
n=len(applications)
with myFile:
    myFields = ['applications', 'NumberOfUses']
    writer = csv.DictWriter(myFile, fieldnames=myFields)
    writer.writeheader()
    for i in range(n):
        if(not ("." in applications[i])):
            writer2 = csv.DictWriter(myFile, fieldnames=myFields)
            writer2.writerow({'applications' : applications[i], 'NumberOfUses': NumberOfUses[i]})



myFile = open('Web/NewAmountOfApplicationUsage.csv', 'w' ,encoding='utf8'  )
n=len(applications)
with myFile:
    myFields = ['applications', 'NumberOfUses']
    writer = csv.DictWriter(myFile, fieldnames=myFields)
    writer.writeheader()
    for i in range(n):
        writer2 = csv.DictWriter(myFile, fieldnames=myFields)
        writer2.writerow({'applications' : applications[i], 'NumberOfUses': NumberOfUses[i]})


myFile = open('Web/NewWebsites.csv', 'w',encoding='utf8'  )
n=len(applications)
Websites_list = []
NumberOfInteraction_list= []
with myFile:
    myFields = ['applications', 'NumberOfUses']
    writer = csv.DictWriter(myFile, fieldnames=myFields)
    writer.writeheader()
    for i in range(n):
        if("." in applications[i]):
            writer2 = csv.DictWriter(myFile, fieldnames=myFields)
            writer2.writerow({'applications' : applications[i], 'NumberOfUses': NumberOfUses[i]})
            Websites_list.append(applications[i])
            NumberOfInteraction_list.append(NumberOfUses[i])


entries2 = []
for app in data['off_facebook_activity']:
    nameNew = app['name']
    if not ("." in nameNew):
        for event in app['events']:
            entries2.append([nameNew, event['type'], event['timestamp']])

OFANew = pd.DataFrame(entries2)
OFANew.columns = ['app_name','event_type', 'timestamp']

applications,NumberOfUses = np.unique(OFANew['app_name'],return_counts = True)
ind=np.argpartition(NumberOfUses, -5)[-5:]
ind
Top10=applications[ind]
ind2=sorted(NumberOfUses[ind], reverse=True)
random=list(zip(Top10, NumberOfUses[ind]))
def take_second(elem):
    return elem[1]

sorted_list = sorted(random, key=take_second, reverse=True)
print(sorted_list[0][0])
myFile = open('Web/TopApps.csv', 'w',encoding='utf8'  )
n=len(sorted_list)
print(n)
with myFile:
    myFields = ['applications','NumberOfUses']
    writer = csv.DictWriter(myFile, fieldnames=myFields)
    writer.writeheader()
    for i in range(n):
        writer2 = csv.DictWriter(myFile, fieldnames=myFields)
        writer2.writerow({'applications' : sorted_list[i][0], 'NumberOfUses': sorted_list[i][1]})



entries3 = []
for app in data['off_facebook_activity']:
    nameNew3 = app['name']
    if ("." in nameNew3):
        for event in app['events']:
            entries3.append([nameNew3, event['type'], event['timestamp']])

OFANew3 = pd.DataFrame(entries3)
OFANew3.columns = ['app_name','event_type', 'timestamp']

applications,NumberOfUses = np.unique(OFANew3['app_name'],return_counts = True)
ind=np.argpartition(NumberOfUses, -5)[-5:]
ind
Top10=applications[ind]
ind2=sorted(NumberOfUses[ind], reverse=True)
random=list(zip(Top10, NumberOfUses[ind]))
def take_second(elem):
    return elem[1]

sorted_list = sorted(random, key=take_second, reverse=True)
print(sorted_list[0][0])
myFile = open('Web/TopWebsites.csv', 'w',encoding='utf8'  )
n=len(sorted_list)
print(n)
with myFile:
    myFields = ['applications','NumberOfUses']
    writer = csv.DictWriter(myFile, fieldnames=myFields)
    writer.writeheader()
    for i in range(n):
        writer2 = csv.DictWriter(myFile, fieldnames=myFields)
        writer2.writerow({'applications' : sorted_list[i][0], 'NumberOfUses': sorted_list[i][1]})





entries4 = []
for app in data['off_facebook_activity']:
    nameNew4 = app['name']
    for event in app['events']:
        entries4.append([nameNew4, event['type'], event['timestamp']])

OFANew4 = pd.DataFrame(entries4)
OFANew4.columns = ['app_name','event_type', 'timestamp']

applications,NumberOfUses = np.unique(OFANew4['app_name'],return_counts = True)
ind=np.argpartition(NumberOfUses, -5)[-5:]
ind
Top10=applications[ind]
ind2=sorted(NumberOfUses[ind], reverse=True)
random=list(zip(Top10, NumberOfUses[ind]))
def take_second(elem):
    return elem[1]

sorted_list = sorted(random, key=take_second, reverse=True)
print(sorted_list[0][0])
myFile = open('Web/TopWebsites&Apps.csv', 'w',encoding='utf8'  )
n=len(sorted_list)
print(n)
with myFile:
    myFields = ['applications','NumberOfUses']
    writer = csv.DictWriter(myFile, fieldnames=myFields)
    writer.writeheader()
    for i in range(n):
        writer2 = csv.DictWriter(myFile, fieldnames=myFields)
        writer2.writerow({'applications' : sorted_list[i][0], 'NumberOfUses': sorted_list[i][1]})





from datetime import datetime
myFile = open('Web/NewApplication_Date.csv', 'w',encoding='utf8'  )
n=len(OFA['timestamp'])
with myFile:
    myFields = ['Application_Name', 'Date','Type','Time' ]
    writer = csv.DictWriter(myFile, fieldnames=myFields)
    writer.writeheader()
    for i in range(n):
        ts = int(OFA['timestamp'][i])
        Date =datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d ')
        DateTime=datetime.utcfromtimestamp(ts).strftime('%H:%M:%S ')
        writer2 = csv.DictWriter(myFile, fieldnames=myFields)
        writer2.writerow({'Application_Name' : OFA['app_name'][i], 'Date': Date, 'Type':OFA['event_type'][i], 'Time':DateTime})


def get_index_positions(list_of_elems, element):

    index_pos_list = []
    index_pos = 0
    while True:
        try:
            # Search for item in list from indexPos to the end of list
            index_pos = list_of_elems.index(element, index_pos)
            # Add the index position in list
            index_pos_list.append(index_pos)
            index_pos += 1
        except ValueError as e:
            break
    return index_pos_list



myFile = open('Web/FinalSummary.csv', 'w',encoding='utf8'  )
n=len(OFA['app_name'])
with myFile:
    myFields = ['Application_Name','Type', 'Number']
    writer = csv.DictWriter(myFile, fieldnames=myFields)
    writer.writeheader()
    writer2 = csv.DictWriter(myFile, fieldnames=myFields)
    resultName = []
    resultType = []
    resultCommon = []
    for i in range(n):
        k=0
        if ((OFA['app_name'][i] in resultName)):

            index_pos_list = get_index_positions(resultName, (OFA['app_name'][i]))
            res_list = [resultType[p] for p in index_pos_list]

            if (not (OFA['event_type'][i]  in res_list )):
                k=1
                resultName.append(OFA['app_name'][i])
                resultType.append(OFA['event_type'][i])
                resultCommon.append(k)

            else:
                for q in index_pos_list:

                    if ( OFA['event_type'][i]  == resultType[q]):

                        resultCommon[q]=resultCommon[q]+1
                        break






        else:
            k=1
            resultName.append(OFA['app_name'][i])
            resultType.append(OFA['event_type'][i])
            resultCommon.append(k)


    n2= len(resultName)
    for i in range(n2):
        writer2.writerow({'Application_Name' : resultName[i],  'Type':resultType[i], 'Number':resultCommon[i]})





# Set web files folder
my_options = {
    'mode': "default", #or "chrome-app",
    'host': 'localhost',
    'port': 8000,
    'chromeFlags': ["--start-fullscreen", "--browser-startup-dialog"]
}


eel.init('web')

@eel.expose                         # Expose this function to Javascript
def say_hello_py(x):
    print('Hello from %s' % x)

say_hello_py('Python World!')


eel.start('index.html', size=(800, 800), options=my_options, suppress_error=True)    # Start

