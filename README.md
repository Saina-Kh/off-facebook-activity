# Facebook Data Visualization tool
This application lets you visualize the data that Facebook collects about you. Facebook gets these data, called Off-Facebook activities, from other companies and organizations. 

# Step 1: Getting the Data
First of all, you need to get the data!:

- Download your Off-Facebook activities in JSON format from your Facebook profile using the following link:
https://www.facebook.com/dyi/?referrer=clear_history.

    - Select only the option "Ads and Businesses" under the category "Information about you". Be sure all the other types of information are not selected!. 
    
    -  Select JSON as format and then click "Create File". Facebook will notify you when the copy is ready, and then you just have to click the  "Download" button to get the file in your computer.

Now, you are ready to download the visualization tool

# Step 2: Installing the visualization tool

- Download the Facebook Data Visualization tool from this website: https://gitlab.com/Saina-Kh/off-facebook-activity/-/archive/master/off-facebook-activity-master.zip

- Unzip the downloaded file in a folder of your choice in your computer 

- Unzip the Off-Facebook activities file downloaded in the previous step, and copy "your_off-facebook_activity.json", located in the  "ads_and_businesses folder", to the "Web" folder of the Visualization tool.

- Copy the whole "Web" folder to the "dist" folder.
- Unzip "Dependencies.zip", "files.zip", and "PythonLibraries.zip" files located in "dist" folder.
- Afer extracting zip files, copy the files form "Dependencies" folder, "files" folder, and "PythonLibraries" to "App" folder.

# Step 3: Visualizing your data

Run the Facebook Data Visualization tool by double-clicking "App.exe" located in "dist/App". After some seconds, an interface similar to the one in the image will appear, and you can start viewing your data!


![alt text](ReadMeImage.png)